package Nested_Loop;

public class NestedLoop {
	private String str = "";
	
	public String nestedLoopOne(int num){
		for (int i = 1; i <= num; i++) {
			str += "*";
			for (int j = 1; j < num; j++) {
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nestedLoopTwo(int num){
		for (int i = 1; i <= num; i++) {
			str += "*";
			for (int j = 1; j < num; j++) {
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nestedLoopThree(int num){
		for (int i = 0; i <= num-1; i++) {
			str += "*";
			for (int j = 1; j <= i; j++) {
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	
	public String nestedLoopFour(int num){
		for (int i = 1; i <= num; i++) {
			for (int j = 1; j <= num+2; j++) {
				if (j%2 == 0){
					str += "*";
				}
				else {
					str += "-";
				}
			}
			str += "\n";
		}
		return str;
	}
	
	public String nestedLoopFive(int num){
		for (int i = 1; i <= num; i++) {
			for (int j = 1; j <= num+2; j++) {
				if ((i + j)%2 == 0){
					str += "*";
				}
				else {
					str += " ";
				}
			}
			str += "\n";
		}
		return str;
	}
	
	public String clearAll(){
		return str = "";
	}
}
