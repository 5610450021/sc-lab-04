package Nested_Loop_Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Nested_Loop_System.NestedControl;

public class NestedLoopFrame {
	NestedControl nc = new NestedControl();
	private JPanel panel1;
	private JPanel panel2;
	private JButton button1;
	private JButton button2;
	private JTextField text1;
	private JTextArea text2;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JComboBox combobox;
	private int num = 0;
	private String str = "";
	
	public void creatFrame(){
		JFrame j = new JFrame("Nested Loop");
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setSize(400, 270);
		j.setLayout(null);
		
		label1 = new JLabel("Choose pattern nested."); 
		label1.setBounds(18, 5, 200, 30);
		
		label2 = new JLabel("Enter number"); 
		label2.setBounds(18, 85, 200, 20);
		
		combobox = new JComboBox();
		combobox.setBounds(16, 40, 150, 26);
		combobox.addItem("Pattern 1");
		combobox.addItem("Pattern 2");
		combobox.addItem("Pattern 3");
		combobox.addItem("Pattern 4");
		combobox.addItem("Pattern 5");
		
		text1 = new JTextField();
		text1.setBounds(16, 116, 150, 23);
		
		button1 = new JButton("OK"); 
		button1.setBounds(33, 160, 120, 25);
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num = Integer.parseInt(text1.getText());
				str = combobox.getSelectedItem().toString();
				text2.setText(nc.choosePattern(str, num));
	        }
		});
		
		button2 = new JButton("CLEAR"); 
		button2.setBounds(33, 195, 120, 25);
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				text2.setText("");
				text1.setText("");
				nc.clearAll();
	        }
		});
		
		text2 = new JTextArea();
		text2.setBounds(5, 40, 180, 186);
		
		label3 = new JLabel("Result"); 
		label3.setBounds(5, 3, 200, 20);
		
		label4 = new JLabel("-- -- -- -- -- -- -- -- -- -- -- --"); 
		label4.setBounds(5, 20, 200, 20);
		
		panel1 = new JPanel();
		panel1.setBounds(5, 6, 190, 233);
		panel1.setLayout(null);
		panel1.setBorder(BorderFactory.createEtchedBorder());
		panel1.add(label1);
		panel1.add(combobox);
		panel1.add(label2);
		panel1.add(text1);
		panel1.add(button1);
		panel1.add(button2);
		
		panel2 = new JPanel();
		panel2.setBounds(200, 6, 190, 233);
		panel2.setLayout(null);
		panel2.setBorder(BorderFactory.createEtchedBorder());
		panel2.add(label3);
		panel2.add(label4);
		panel2.add(text2);
		
		j.add(panel1);
		j.add(panel2);
		j.setVisible(true);
		j.setResizable(false);
	}
}
