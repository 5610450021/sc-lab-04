package Nested_Loop_System;

import Nested_Loop.NestedLoop;
import Nested_Loop_Frame.NestedLoopFrame;

public class NestedControl {
	public static void main(String[] args) {
		NestedLoopFrame nf = new NestedLoopFrame();
		nf.creatFrame();
	}
	
	NestedLoop nl = new NestedLoop();
	String s = "";
	
	public String choosePattern(String str, int num){
		if (str == "Pattern 1"){
			s = nl.nestedLoopOne(num);
		}
		else if (str == "Pattern 2"){
			s = nl.nestedLoopTwo(num);
		}
		else if (str == "Pattern 3"){
			s = nl.nestedLoopThree(num);
		}
		else if (str == "Pattern 4"){
			s = nl.nestedLoopFour(num);
		}
		else {
			s = nl.nestedLoopFive(num);
		}
	return s;
	}	
	
	public String clearAll(){
		return nl.clearAll();
	}
}
